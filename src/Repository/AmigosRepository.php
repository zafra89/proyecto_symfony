<?php

namespace App\Repository;

use App\Entity\Amigos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Amigos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Amigos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Amigos[]    findAll()
 * @method Amigos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AmigosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Amigos::class);
    }

    // /**
    //  * @return Amigos[] Returns an array of Amigos objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Amigos
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
