<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response; //Para usar los Response
use Symfony\Component\Routing\Annotation\Route; //Para usar las annotation @Route


class MyController {
  /**
   * @Route("/check/{item}")
   */
  public function funcionEsParOLowerUpper($item) {
    $comprobarSiNumerico = is_numeric($item);
    if ($comprobarSiNumerico) {
      if ($item % 2 == 0) {
        return new Response("El número $item es PAR");
      } else {
        return new Response("El número $item NO es par");
      }
    } else {
      $arrayItem = str_split($item);
      $newArrayItem = [];
      foreach($arrayItem as $index => $it) {
        if ($index % 2 == 0) {
          array_push($newArrayItem, strtolower($it));
        } else {
          array_push($newArrayItem, strtoupper($it));
        }
      }
      return new Response(implode('', $newArrayItem));
    }
  }

  /**
   * @Route("/usuario/{nombre}/edad/{edad}", requirements={"edad"="\d+"})
   */
  public function user($nombre, $edad) {
    return new Response("Hola $nombre, te damos la bienvenida. Tienes $edad años");
  }
}
?>