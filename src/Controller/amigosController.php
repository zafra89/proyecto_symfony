<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route; //Para usar las annotation @Route
use Dotrine\ORM\EntityManagerInterface;

use App\Entity\Amigos;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface as ORMEntityManagerInterface;

class amigosController extends AbstractController { // 'extends AbstractController' Necesario con twig

  /**
   * @Route("/nuevoamigo")
   */
  public function nuevoAmigo(ORMEntityManagerInterface $doctrine) {
    $amigo1 = new Amigos();
    $amigo1->setNombre('Ana Luz');
    $amigo1->setEdad(28);
    $doctrine->persist($amigo1);
    $doctrine->flush();

    $amigo2 = new Amigos();
    $amigo2->setNombre('Myriam');
    $amigo2->setEdad(29);
    $doctrine->persist($amigo2);
    $doctrine->flush();
  }

  /**
   * @Route("/showamigos")
   */
  public function showAmigo(ORMEntityManagerInterface $doctrine) {
    $this->nuevoAmigo($doctrine);
    $repo = $doctrine->getRepository(Amigos::class);
    $amigo = $repo->findAll();

    return $this->render('listadoAmigos.html.twig', ['amigos' => $amigo]);
  }
}