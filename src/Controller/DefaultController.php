<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response; //Para usar los Response
use Symfony\Component\Routing\Annotation\Route; //Para usar las annotation @Route
use Dotrine\ORM\EntityManagerInterface;

use App\Entity\Album;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface as ORMEntityManagerInterface;

class DefaultController extends AbstractController
{ // 'extends AbstractController' Necesario con twig
  /**
   * @Route("/inicio")
   */
  /*public function homepage() {
    return new Response("Hola Mundo");
  }*/

  /**
   * @Route("/usuario/{nombre}/edad/{edad}", requirements={"edad"="\d+"})
   */
  /*public function user($nombre, $edad) {
    return new Response("Hola $nombre, te damos la bienvenida. Tienes $edad años");
  }*/
  /**
   * @Route("/showalbums")
   */
  public function showAlbums(ORMEntityManagerInterface $doctrine) {
    $repo = $doctrine->getRepository(Album::class);
    $album = $repo->findAll();

    return $this->render("favAlbums.html.twig", ['albums' => $album]);
    /*$albums = [
      ['name' => 'The Glow Pt. 2', 'img' => 'https://upload.wikimedia.org/wikipedia/en/7/72/The_Glow_Pt._2_%28Front_Cover%29.png', 'artist' => 'The Microphones', 'year' => 2001],
      ['name' => '69 Love Songs', 'img' => 'https://static.stereogum.com/uploads/2019/09/The-Magnetic-Fields-69-Love-Songs-1568042488-640x640.png', 'artist' => 'The Magnetic Fields', 'year' => 1999],
      ['name' => 'Dopethrone', 'img' => 'https://images-na.ssl-images-amazon.com/images/I/71fnAZ4oG4L._SX425_.jpg', 'artist' => 'Electric Wizard', 'year' => 2000],
      ['name' => 'Lift Your Skinny Fists Like Antennas to Heaven', 'img' => 'https://bcstore.bcoredisc.com/wp-content/uploads/2020/02/godspeed-lift.jpg', 'artist' => 'Godspeed You! Black Emperor', 'year' => 2000],
      ['name' => 'Deathconsciousness', 'img' => 'https://img.discogs.com/gzPzzyhEKRmykldTb-Hu2m5MqYM=/fit-in/600x600/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-1308408-1422926327-9891.jpeg.jpg', 'artist' => 'Have a Nice Life', 'year' => 2008],
      ['name' => 'In Rainbows', 'img' => 'https://images-na.ssl-images-amazon.com/images/I/A1MwaIeBpwL._SL1500_.jpg', 'artist' => 'Radiohead', 'year' => 2007],
      ['name' => 'Forever Changes', 'img' => 'https://www.elquintobeatle.com/wp-content/uploads/2017/12/love-forever-changes-1.jpg', 'artist' => 'Love', 'year' => 1967],
      ['name' => 'In the Aeroplane Over the Sea', 'img' => 'https://images-na.ssl-images-amazon.com/images/I/81bCY4xIiuL._SX466_.jpg', 'artist' => 'Neutral Milk Hotel', 'year' => 1998],
      ['name' => 'Laughing Stock', 'img' => 'https://images-na.ssl-images-amazon.com/images/I/71tOFUGVgOL._SL1205_.jpg', 'artist' => 'Talk Talk', 'year' => 1991],
      ['name' => 'Lateralus', 'img' => 'https://3.bp.blogspot.com/-lvJGO0KqEq4/TyQSyqUQlkI/AAAAAAAAAHc/7SavPpnfHOo/s1600/tl.jpg', 'artist' => 'Tool', 'year' => 2001],
      ['name' => 'Astral Weeks', 'img' => 'https://images-na.ssl-images-amazon.com/images/I/71KeZC9QIEL._SL1400_.jpg', 'artist' => 'Van Morrison', 'year' => 1968],
      ['name' => 'Yank Crime', 'img' => 'https://img.discogs.com/Rh0cYkx5Sh-XCsIvBuSWdlqxAy0=/fit-in/600x602/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-1011657-1392251225-1331.jpeg.jpg', 'artist' => 'Drive Like Jehu', 'year' => 1994],

    ];*/
  }

  /**
   * @Route("/newalbum")
   */

   public function newAlbum(ORMEntityManagerInterface $doctrine) {
    $album1 = new Album();
    $album1->setName('The Glow Pt. 2');
    $album1->setArtist('The Microphones');
    $album1->setYear(2001);
    $album1->setImg('https://upload.wikimedia.org/wikipedia/en/7/72/The_Glow_Pt._2_%28Front_Cover%29.png');
    $doctrine->persist($album1);
    $doctrine->flush();

    $album2 = new Album();
    $album2->setName('Lateralus');
    $album2->setArtist('Tool');
    $album2->setYear(2001);
    $album2->setImg('https://3.bp.blogspot.com/-lvJGO0KqEq4/TyQSyqUQlkI/AAAAAAAAAHc/7SavPpnfHOo/s1600/tl.jpg');
    $doctrine->persist($album2);
    $doctrine->flush();

    $album3 = new Album();
    $album3->setName('In Rainbows');
    $album3->setArtist('Radiohead');
    $album3->setYear(2007);
    $album3->setImg('https://images-na.ssl-images-amazon.com/images/I/A1MwaIeBpwL._SL1500_.jpg');
    $doctrine->persist($album3);
    $doctrine->flush();
   }
}